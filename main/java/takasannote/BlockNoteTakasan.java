package takasannote;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockNote;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityNote;
import net.minecraft.util.IIcon;
import net.minecraft.world.IWorldAccess;
import net.minecraft.world.World;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class BlockNoteTakasan extends BlockNote {
	
	static private IIcon takaFace;
	
	public BlockNoteTakasan() {
		super();
		this.setBlockTextureName("takasannote:noteblock");
	}
	
	@Override
	public void registerBlockIcons(IIconRegister iconRegister) {
		super.registerBlockIcons(iconRegister);
		this.takaFace = iconRegister.registerIcon("takasannote:takaface");
	}

    /**
     * Returns a new instance of a block's tile entity class. Called on placing the block.
     */
    public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_)
    {
        return new TileEntityNoteTakasan();
    }

    @SideOnly(Side.CLIENT)
    public boolean onBlockEventReceived(World world, int x, int y, int z, int type, int pitch)
    {
        float f = (float)Math.pow(2.0D, (double)((pitch)-12) / 12.0D);
        String s = "hora";
        
        // typeによって s を変える
        if (type == 1)
        {
            //s = "bd";
        	s = "aaa";
        }

        if (type == 2)
        {
            //s = "snare";
        	s = "hohun";
        }

        if (type == 3)
        {
            //s = "hat";
        	s = "nee";
        }

        if (type == 4)
        {
            //s = "bassattack";
        	s = "taa";
        }


        // 鷹さん音声を流す。
        world.playSoundEffect((double)x + 0.5D, (double)y + 0.5D, (double)z + 0.5D, "takasannote:" + s, 3.0F, f);
        spawnTakaFace(world, x, y, z, pitch);
        return true;
    }

	private void spawnTakaFace(World world, int x, int y, int z, int pitch) {
		Minecraft mc = net.minecraft.client.Minecraft.getMinecraft();
		// 鷹さんの顔を出す。音の高さによって飛ぶ高さがことなる
		//world.spawnParticle("note", (double)x + 0.5D, (double)y + 1.2D, (double)z + 0.5D, 0, pitch, 0);
		if (mc != null && mc.renderViewEntity != null && mc.effectRenderer != null)
        {
			EntityTakaFaceFx fx = new EntityTakaFaceFx(world, (double)x + 0.5D, (double)y + 1.2D, (double)z + 0.5D, 0, pitch, 0, this.takaFace);
			mc.effectRenderer.addEffect(fx);
        }
	}
}
