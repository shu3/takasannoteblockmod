package takasannote;

import net.minecraft.block.Block;
import net.minecraft.block.BlockNote;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = TakasanNoteMod.MODID, version = TakasanNoteMod.VERSION)
public class TakasanNoteMod {
    public static final String MODID = "takasannote";
    public static final String VERSION = "1.0";
    public static Block takasanBlock;

    
    @EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
    	this.takasanBlock = (new BlockNoteTakasan()).setBlockName("blockNoteTakasan").setHardness(0.8F);
    	GameRegistry.registerBlock(takasanBlock, "blockNoteTakasan");
    	GameRegistry.registerTileEntity(TileEntityNoteTakasan.class, "TakasanMusic");
    	
    	// レシピ追加
    	GameRegistry.addShapedRecipe(
    			new ItemStack(this.takasanBlock, 1),
    			new Object[] {" R ", "RSR", " R ",
    				Character.valueOf('R'), Items.redstone,
    				Character.valueOf('S'), Items.water_bucket
    			});

	}
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
		// some example code
    }
}
